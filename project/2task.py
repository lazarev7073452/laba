import os
import psycopg2


def conn(query1, query2):
    try:
        connection = psycopg2.connect(os.getenv("POSTGRES_URL"))
        cursor = connection.cursor()
        return exec(cursor,  query1), exec(cursor,  query2)
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        connection.close()

def exec(cursor, query):
    cursor.execute(query)
    return cursor.fetchone()


if __name__ == '__main__':
    query1 = """
        SELECT name, surname, middle_name, birth FROM students
        ORDER BY birth DESC
        LIMIT 1;
    """

    query2 = """
        SELECT name, surname, middle_name, birth FROM students
        ORDER BY birth ASC
        LIMIT 1;
    """

    youngster, elder = conn(query1, query2)

    if youngster and elder:
        print(f"Самый молодой студент: {youngster[0]} {youngster[1]} {youngster[2]} с датой рождения {youngster[3]}")
        print(f"Самый старший студент: {elder[0]} {elder[1]} {elder[2]} с датой рождения {elder[3]}")
    else:
        print("Данные не найдены.")
