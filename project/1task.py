import os
import random
import string


def generate_password(length=12):
    characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for _ in range(length))
    return password


if __name__ == "__main__":
    try:
        length = int(os.getenv("LENGTH", 8))
        if 8 <= length <= 32:
            password = generate_password(length)
            print("Сгенерированный пароль:", password)
        else:
            print("Ошибка: Длина пароля должна быть от 8 до 32 символов.")
    except ValueError:
        print("Ошибка: Введите целое число.")
