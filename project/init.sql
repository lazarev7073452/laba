CREATE TABLE IF NOT EXISTS students (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    middle_name VARCHAR(255) NOT NULL,
    birth DATE NOT NULL
    );

CREATE TABLE IF NOT EXISTS course (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    lecturer VARCHAR(255) NOT NULL
    );

CREATE TABLE IF NOT EXISTS report_card (
    id SERIAL PRIMARY KEY,
    id_student INT NOT NULL,
    id_lesson INT NOT NULL,
    date DATE NOT NULL,
    mark INT,
    FOREIGN KEY (id_student) REFERENCES students(id),
    FOREIGN KEY (id_lesson) REFERENCES course(id)
    );


INSERT INTO students (name, surname, middle_name, birth) VALUES
     ('Михаил', 'Воронин', 'Даниилов', '2002-04-02'),
     ('Александра', 'Софронова', 'Михайловна', '2002-06-07'),
     ('Елена', 'Соловьева', 'Артёмовна', '2002-01-19'),
     ('Андрей', 'Иванов', 'Ярославович', '2002-12-01'),
     ('Тимур', 'Беляева', 'Ильич', '2002-08-21');

INSERT INTO course (name, lecturer) VALUES
     ('Организация обработки больших данных', 'Лебедев Артём Сергеевич'),
     ('Модели разёртывания удалённых сервисов', 'Сачков Валерий Евгеньевич'),
     ('Обработка потоковой информации интернет-вещей', 'Котилевец Игорь Денисович'),
     ('Инструментальный анализ защинённости', 'Литвин Игорь Анатольевич'),
     ('Системы ведения хранилищ данных', 'Лебедев Артём Сергеевич');



INSERT INTO report_card (id_student, id_lesson, date, mark) VALUES
    (1, 1, '2022-01-13', 3),
    (2, 1, '2022-01-13', 5),
    (3, 1, '2022-01-13', 4),
    (4, 1, '2022-01-13', 5),
    (5, 1, '2022-01-13', 2),
    (1, 2, '2022-01-22', 3),
    (2, 2, '2022-01-22', 5),
    (3, 2, '2022-01-22', 2),
    (4, 2, '2022-01-22', 4),
    (5, 2, '2022-01-22', 5),
    (1, 3, '2022-01-15', 5),
    (2, 3, '2022-01-15', 5),
    (3, 3, '2022-01-15', 4),
    (4, 3, '2022-01-15', 5),
    (5, 3, '2022-01-15', 4),
    (1, 4, '2022-01-10', 3),
    (2, 4, '2022-01-10', 4),
    (3, 4, '2022-01-10', 4),
    (4, 4, '2022-01-10', 5),
    (5, 4, '2022-01-10', 5),
    (1, 5, '2022-01-18', 5),
    (2, 5, '2022-01-18', 4),
    (3, 5, '2022-01-18', 4),
    (4, 5, '2022-01-18', 4),
    (5, 5, '2022-01-18', 3)